extends "res://game/parts/Part.gd"

export(float) var bullet_speed = 500
export(float) var fire_cooldown = 1
export(float) var bullet_damage = 0.5
export(float) var bullet_penetration = 1

func _process(_delta):
	if(enabled and can_shoot and Input.is_action_pressed(button)):
		shoot()
		can_shoot = false
		$ShootCooldown.start()

func shoot():
	var b = preload("res://game/Bullet.tscn").instance()
	parent.add_child(b)
	b.global_position = $ShootPos.global_position
	b.rotation = $ShootPos.global_rotation
	b.velocity = Vector2.UP.rotated(b.rotation)*bullet_speed+player.velocity
	#add tangential velocity
	b.velocity += Vector2.RIGHT.rotated(global_rotation)*(player.angular_velocity*($ShootPos.global_position.distance_to(player.global_position)))
	b.damage = bullet_damage*damage_mult
	b.penetration = bullet_penetration
	play_sfx(4, 3)

var can_shoot = true
func _on_ShootCooldown_timeout():
	can_shoot = true

var enabled = true
func disable():
	enabled = false

func set_fire_rate_mult(new):
	fire_rate_mult = new
	if(is_inside_tree()):
		$ShootCooldown.wait_time = fire_cooldown*fire_rate_mult

func play_sfx(sound:int, vol:float):
	var sfx = preload("res://game/SFXPlayer.tscn").instance()
	sfx.set_sound(sound)
	sfx.set_volume(vol)
	parent.add_child(sfx)
	sfx.global_position = global_position
