extends "res://game/parts/Part.gd"

export(float) var damage_per_second = 2
export(float) var cooldown = 3
var shooting = false
func _draw():
	if(shooting):
		draw_line(Vector2(0, $LaserArea.position.y), Vector2(0, $LaserArea.position.y-$LaserArea/CollisionShape2D.shape.extents.y*2), Color(1, 1, 1), 10, true)

var can_shoot = true
func _on_ShootCooldown_timeout():
	can_shoot = true

func _on_LaserTimer_timeout():
	shooting = false
	$LaserPlayer.stop()
	update()
	$ShootCooldown.start()

func _process(delta):
	if(enabled and can_shoot and Input.is_action_pressed(button)):
		can_shoot = false
		shooting = true
		$LaserPlayer.play()
		update()
		$LaserTimer.start()
	if(shooting):
		for b in $LaserArea.get_overlapping_bodies():
			if(b.is_in_group("enemy")):
				b.hit(delta*damage_per_second*damage_mult)
		for a in $LaserArea.get_overlapping_areas():
			if(a.is_in_group("enemy")):
				a.hit(delta*damage_per_second*damage_mult)

func set_fire_rate_mult(new):
	fire_rate_mult = new
	if(is_inside_tree()):
		$ShootCooldown.wait_time = cooldown*new

var enabled = true
func disable():
	enabled = false
