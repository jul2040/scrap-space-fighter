extends "res://game/parts/Shooter.gd"

func shoot():
	var b = preload("res://game/Mine.tscn").instance()
	b.position = $ShootPos.global_position
	b.rotation = global_rotation
	b.velocity = Vector2.UP.rotated(b.rotation)*bullet_speed+player.velocity
	#add tangential velocity
	b.velocity += Vector2.RIGHT.rotated(global_rotation)*(player.angular_velocity*($ShootPos.global_position.distance_to(player.global_position)))
	b.position += Vector2.UP.rotated(b.rotation)*30
	b.damage = bullet_damage*damage_mult
	parent.add_child(b)
	play_sfx(4, 3)
