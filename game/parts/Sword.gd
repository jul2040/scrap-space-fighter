extends "res://game/parts/Part.gd"

export(float) var damage = 0.5
export(float) var repel_force = 300

func _on_SwordArea_body_entered(body):
	if(body.is_in_group("enemy")):
		body.velocity += (body.global_position-global_position).normalized()*repel_force
		body.hit(damage*damage_mult)

func _on_SwordArea_area_entered(area):
	_on_SwordArea_body_entered(area)
