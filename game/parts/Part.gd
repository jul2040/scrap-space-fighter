extends Node2D

export(float) var speed_mult = 1
export(float) var rot_speed_mult = 1
export(float) var fire_rate_mult = 1 setget set_fire_rate_mult
export(float) var damage_mult = 1

var button:String
var parent
var player

func disable():
	pass

func set_fire_rate_mult(new):
	fire_rate_mult = new
