extends AudioStreamPlayer2D

func set_volume(v:float):
	volume_db = linear2db(v)

func set_sound(s:int):
	stream = GameInfo.sfx[s]

func _on_SFXPlayer_finished():
	queue_free()
