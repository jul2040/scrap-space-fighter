tool
extends Node2D

var enemy_scenes = [
	preload("res://game/enemies/Chaser.tscn"),
	preload("res://game/enemies/Shooter.tscn"),
	preload("res://game/enemies/ChaserShooter.tscn")
]
func _ready():
	if(Engine.editor_hint):
		return
	MusicManager.music(0)
	GameInfo.connect("score_changed", self, "update_score")
	update_score()
	ShipEditor.connect("done", self, "done_editing")
	for _i in range(clamp(GameInfo.difficulty+3, 3, 8)):
		var e
		if(GameInfo.difficulty < 1):
			e = enemy_scenes[randi()%(len(enemy_scenes)-2)].instance()
		elif(GameInfo.difficulty < 3):
			e = enemy_scenes[randi()%(len(enemy_scenes)-1)].instance()
		else:
			e = enemy_scenes[randi()%(len(enemy_scenes))].instance()
		e.player = $Player
		e.position = (Vector2.UP*(level_radius*rand_range(0.5, 1))).rotated(rand_range(0,2*PI))
		$Enemies.add_child(e)
		e.connect("die", self, "on_enemy_die")
	for _i in range(5):
		var a = preload("res://game/enemies/Asteroid.tscn").instance()
		a.position = (Vector2.UP*(level_radius*rand_range(0.5, 1))).rotated(rand_range(0,2*PI))
		a.velocity = (Vector2.UP*100).rotated(rand_range(0, 2*PI))
		add_child(a)
	var equipped = ShipEditor.equipped
	for i in range(len(equipped)):
		$Player.set_part(i, equipped[i])
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	get_tree().paused = true

func on_enemy_die():
	if($Player.health <= 0 or ShipEditor.is_editing()):
		return
	if($Enemies.get_child_count() <= 1):
		end_level()

export(float) var level_radius = 500 setget set_radius

func set_radius(new):
	if(not Engine.editor_hint):
		yield(self, "ready")
	level_radius = new
	#$Stars.process_material.emission_sphere_radius = new
	#$Stars.visibility_rect = Rect2(-new, -new, new*2, new*2)
	$BlackHole/CollisionShape2D.shape.radius = new

var parts = []
func end_level():
	var new_part = PartInfo.get_random_part()
	$CanvasLayer/Display/ItemLabel.text = PartInfo.get_part_name(new_part)
	parts = [new_part]
	$CanvasLayer/Display.visible = true
	$CanvasLayer/DisplayTimer.start()
	$Player.disable()

func done_editing():
	get_tree().reload_current_scene()

func _on_DisplayTimer_timeout():
	GameInfo.difficulty += 1
	get_tree().paused = true
	ShipEditor.edit(parts)

func _process(_delta):
	if(Engine.editor_hint):
		return
	if(not ready):
		var t = $CanvasLayer/ReadyTimer.time_left
		$CanvasLayer/Ready.text = str(ceil(t))
	else:
		if(Input.is_action_just_pressed("pause")):
			get_tree().paused = !get_tree().paused
			$CanvasLayer/Paused.visible = get_tree().paused

var ready = false
func _on_ReadyTimer_timeout():
	ready = true
	get_tree().paused = false
	$CanvasLayer/Ready.text = "GO"
	$CanvasLayer/GoTimer.start()

func _on_GoTimer_timeout():
	$CanvasLayer/Ready.visible = false

func update_score():
	$CanvasLayer/ScoreLabel.text = "%06d"%GameInfo.score
