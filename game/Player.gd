extends KinematicBody2D

export(float) var forward_accel = 200
export(float) var back_accel = 150
export(float) var turn_accel = 4

export(float) var linear_damp = 0.2
export(float) var angular_damp = 1

var velocity:Vector2 = Vector2()
var angular_velocity:float = 0.0

var black_hole = null

func engine_to(delta:float, to:float):
	$EnginePlayer.volume_db = linear2db(lerp(db2linear($EnginePlayer.volume_db), to, delta))

func _physics_process(delta:float):
	#input
	if(enabled):
		if(Input.is_action_pressed("up")):
			velocity += Vector2.UP.rotated(rotation)*(forward_accel*delta)
			engine_to(delta*9, 3)
		if(Input.is_action_pressed("down")):
			velocity += Vector2.DOWN.rotated(rotation)*(back_accel*delta)
			engine_to(delta*9, 3)
		if(Input.is_action_pressed("left")):
			angular_velocity -= turn_accel*delta
		if(Input.is_action_pressed("right")):
			angular_velocity += turn_accel*delta
	engine_to(delta*7, 0)
	#apply black hole force
	if(black_hole != null):
		velocity += ((black_hole.position-position))*delta
	#damping
	angular_velocity -= angular_velocity*(angular_damp*delta)
	velocity -= velocity*(linear_damp*delta)
	#apply velocities
	rotate(angular_velocity*delta)
	move_and_collide(velocity*delta)

func _process(_delta):
	$Node/Healthbar.position = global_position

var fire_rate_mult = 1
var damage_mult = 1
func set_part(slot:int, part_id:int):
	if(part_id == 0):
		return
	var part = PartInfo.get_scene(part_id).instance()
	forward_accel *= part.speed_mult
	back_accel *= part.speed_mult
	turn_accel *= part.rot_speed_mult
	fire_rate_mult *= part.fire_rate_mult
	damage_mult *= part.damage_mult
	part.parent = get_parent()
	part.button = "shoot%d"%(slot+1)
	part.player = self
	$Parts.get_children()[slot].add_child(part)
	for i in $Parts.get_children():
		for p in i.get_children():
			p.fire_rate_mult = fire_rate_mult
			p.damage_mult = damage_mult

var enabled = true
func disable():
	enabled = false
	for p in $Parts.get_children():
		for part in p.get_children():
			part.disable()

var health:float = 1
func hit(damage:float):
	if(invun):
		return false
	invun = true
	$AnimationPlayer.play("invun")
	health -= damage
	$Node/Healthbar.health = health
	if(health <= 0 and enabled):
		disable()
		ShipEditor.reset()
		$AnimationPlayer.play("die")
		MusicManager.music(1)
	else:
		play_sfx(2)
	return true

var invun = false
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "die"):
		get_tree().change_scene("res://menus/DeathMenu.tscn")
	elif(anim_name == "invun"):
		invun = false

func play_sfx(sound:int):
	var sfx = preload("res://game/SFXPlayer.tscn").instance()
	sfx.set_sound(sound)
	get_parent().add_child(sfx)
	sfx.global_position = global_position
