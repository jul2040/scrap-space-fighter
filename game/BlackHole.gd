extends Area2D

var radius = 500
func _on_BlackHole_body_exited(body):
	if(body.is_in_group("black_hole")):
		body.black_hole = self

func _on_BlackHole_body_entered(body):
	if(body.is_in_group("black_hole")):
		body.black_hole = null

func _on_BlackHole_area_entered(area):
	if(area.is_in_group("black_hole")):
		area.black_hole = null

func _on_BlackHole_area_exited(area):
	if(area.is_in_group("black_hole")):
		area.black_hole = self
