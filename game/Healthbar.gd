extends Node2D

var health:float = 1 setget set_health
const size = 60
func _draw():
	if(health > 0):
		draw_line(Vector2(-size-4, -size+4), Vector2(size+4, -size+4), Color(1, 1, 1), 4, true)
		draw_line(Vector2(-size-4, -size-4), Vector2(size+4, -size-4), Color(1, 1, 1), 4, true)
		draw_line(Vector2(-size-2, -size-4), Vector2(-size-2, -size+4), Color(1, 1, 1), 4, true)
		draw_line(Vector2(size+2, -size-4), Vector2(size+2, -size+4), Color(1, 1, 1), 4, true)
		draw_line(Vector2(-size, -size), Vector2(-size+(health*(size*2)), -size), Color(1, 1, 1), 4, true)

func set_health(new):
	health = new
	update()
