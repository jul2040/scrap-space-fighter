extends Area2D

export(float) var damp = 0.5
export(float) var explode_force = 100

var damage = 0.5

func _on_Mine_area_entered(area):
	_on_Mine_body_entered(area)

func _on_Mine_body_entered(body):
	if(body.is_in_group("enemy") or body.is_in_group("player")):
		velocity = Vector2()
		var overlapping = $Explosion.get_overlapping_bodies()
		overlapping.append_array($Explosion.get_overlapping_areas())
		for b in overlapping:
			if(b.is_in_group("enemy") or b.is_in_group("player")):
				b.hit(damage)
				b.velocity += (position-b.position).normalized()*explode_force
		$ExplodeParticles.emitting = true
		$ExplodeTimer.start()
		$Sprite.visible = false

var black_hole = null
var velocity:Vector2 = Vector2()
func _physics_process(delta):
	#apply black hole force
	if(black_hole != null):
		velocity += ((black_hole.position-position))*delta
	velocity -= velocity*(damp*delta)
	position += velocity*delta

func _on_FreeTimer_timeout():
	queue_free()

func _on_ExplodeTimer_timeout():
	queue_free()
