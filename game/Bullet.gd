extends Area2D

var penetration:int = 5
var damage:float = 0.55
func _draw():
	draw_line(Vector2.DOWN*10, Vector2.UP*10, Color(1, 1, 1), 4)

var velocity:Vector2 = Vector2()
func _physics_process(delta):
	position += (velocity*delta)

func _on_Bullet_body_entered(body):
	if(body.is_in_group("enemy")):
		body.hit(damage)
		penetration -= 1
		if(penetration <= 0):
			queue_free()

func _on_FreeTimer_timeout():
	queue_free()
