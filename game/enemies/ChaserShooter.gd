extends "res://game/enemies/Chaser.gd"

export(float) var bullet_speed = 200

func _on_ShootTimer_timeout():
	var b = preload("EnemyBullet.tscn").instance()
	b.position = position
	b.velocity = bullet_speed*(((player.position-position)+(player.velocity*0.5)).normalized())+velocity	
	get_node("../..").add_child(b)
