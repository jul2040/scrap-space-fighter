extends Node2D

var health:float = 1 setget set_health
func _draw():
	#draw health bar
	draw_arc(Vector2(), 50, PI/2, 2*PI*health+PI/2, 24, Color(1, 1, 1), 4, true)

func set_health(new):
	health = new
	update()
