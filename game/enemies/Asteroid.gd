extends Area2D

export(float) var size = 32
export(float) var num_points = 8
export(float) var variance = 8

func _ready():
	$CollisionShape2D.shape.radius = size

func _draw():
	#draw_circle(Vector2(), size, Color(1, 1, 1))
	var points = []
	for i in range(num_points):
		points.append((Vector2.UP*rand_range(size-variance, size+variance)).rotated((float(i)/num_points)*(PI*2)))
	for d in range(-1, len(points)-1, 1):
		draw_line(points[d], points[d+1], Color(1, 1, 1), 4, true)

var black_hole = null
var velocity:Vector2 = Vector2()
func _physics_process(delta):
	#apply black hole force
	if(black_hole != null):
		velocity += ((black_hole.position-position))*delta
	position += velocity*delta

func _on_Asteroid_body_entered(body):
	if(body.is_in_group("player")):
		if(body.hit(0.25)):
			split()

func _on_Asteroid_area_entered(area):
	if(area.is_in_group("bullet")):
		area.penetration -= 1
		if(area.penetration <= 0):
			area.queue_free()
		split()

func hit(_damage):
	split()

func split():
	GameInfo.score += 1
	if(size > 16):
		var r = rand_range(0, 2*PI)
		var a1 = GameInfo.asteroid_scene.instance()
		a1.position = position + (Vector2.LEFT.rotated(r))*size
		a1.velocity = velocity + (Vector2.LEFT.rotated(r))*50
		a1.size = size-8
		var a2 = GameInfo.asteroid_scene.instance()
		a2.position = position + (Vector2.RIGHT.rotated(r))*size
		a2.velocity = velocity + (Vector2.RIGHT.rotated(r))*50
		a2.size = size-8
		get_parent().call_deferred("add_child",a1)
		get_parent().call_deferred("add_child",a2)
	play_sfx(1)
	queue_free()

func play_sfx(sound:int):
	var sfx = preload("res://game/SFXPlayer.tscn").instance()
	sfx.set_sound(sound)
	get_parent().add_child(sfx)
	sfx.global_position = global_position
