extends Area2D

var penetration:int = 1
var damage:float = 0.5
func _draw():
	draw_arc(Vector2(), 5, 0, 2*PI, 24, Color(1, 1, 1), 2, true)

var velocity:Vector2 = Vector2()
func _physics_process(delta):
	position += (velocity*delta)

func _on_EnemyBullet_body_entered(body):
	if(body.is_in_group("player")):
		if(body.hit(damage)):
			penetration -= 1
			if(penetration <= 0):
				queue_free()

func _on_FreeTimer_timeout():
	queue_free()
