extends KinematicBody2D

export(float) var accel = 200
export(float) var damp = .5
export(float) var repel_force = 50

var health:float = 1

func _ready():
	_process(0)

var black_hole
var player
var velocity:Vector2 = Vector2()
func _physics_process(delta):
	rotation = (Vector2.UP).angle_to(player.position-position)
	velocity += (player.position-position).normalized()*(accel*delta)
	#apply black hole force
	if(black_hole != null):
		velocity += ((black_hole.position-position))*delta
	#repel forces
	for r in repel:
		var diff:Vector2 = (position-r.position)
		velocity += diff.normalized()*(delta*repel_force)
	velocity -= velocity*(damp*delta)
	move_and_collide(velocity*delta)

func _on_PlayerDetector_body_entered(body):
	if(body.is_in_group("player")):
		if(body.hit(health*0.5)):
			play_sfx(0)
			queue_free()

var repel = []
func _on_RepelArea_body_entered(body):
	if(body.is_in_group("enemy") and body != self):
		repel.append(body)

func _on_RepelArea_body_exited(body):
	if(body.is_in_group("enemy") and body != self):
		repel.erase(body)

signal die
func _exit_tree():
	emit_signal("die")

func _process(_delta):
	$Node/CircularHealthbar.position = global_position

export(int) var score = 5
func hit(damage):
	health -= damage
	$Node/CircularHealthbar.health = health
	if(health <= 0):
		GameInfo.score += score
		play_sfx(0)
		queue_free()
	else:
		play_sfx(1)

func play_sfx(sound:int):
	var sfx = preload("res://game/SFXPlayer.tscn").instance()
	sfx.set_sound(sound)
	get_node("../..").add_child(sfx)
	sfx.global_position = global_position
