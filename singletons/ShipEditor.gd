extends CanvasLayer

var equipped = [1, 1, 0, 0]
var new_parts = []

var part_pool = []
onready var selectors = [
		$ShipEditor/Selectors/Selector1,
		$ShipEditor/Selectors/Selector2,
		$ShipEditor/Selectors/Selector3,
		$ShipEditor/Selectors/Selector4
]
func edit(new):
	MusicManager.music(2)
	new_parts = new
	$ShipEditor.visible = true
	init_selectors()
	part_pool.append_array(new_parts)
	for i in equipped:
		if(i != 0):
			part_pool.append(i)
	$ShipEditor/Selectors/Selector1.grab_focus()
	for s in selectors:
		selector_changed(0, s)
	selector_changed(0, selectors[0])

func _ready():
	for selector in selectors:
		selector.connect("selected_changed", self, "selector_changed", [selector])
		selector.connect("focus_entered", self, "selector_focused", [selector])

func init_selectors():
	var options = [0]
	for e in equipped:
		if(e != 0):
			options.append(e)
	options.append_array(new_parts)
	var i:int = 0
	for selector in selectors:
		selector.options = options
		selector.current_option = 0
		i += 1
	i = 0
	for selector in selectors:
		if(equipped[i] == 0):
			selector.current_option = 0
		else:
			var o:int = 1
			for e in range(i):
				if(equipped[e] != 0):
					o += 1
			selector.current_option = o
		i += 1

func selector_changed(_option, selector):
	$ShipEditor/Right/DescLabel.text = selector.get_desc()
	var id = $ShipEditor/Selectors.get_children().find(selector)
	var tex = PartInfo.get_texture(selector.get_selected())
	var rect = $ShipEditor/Display.get_child(id)
	if(tex != null):
		rect.visible = true
		rect.texture = tex
	else:
		rect.visible = false

func selector_focused(selector):
	$ShipEditor/Right/DescLabel.text = selector.get_desc()

signal done
func _on_Done_pressed():
	equipped = []
	for selector in selectors:
		equipped.append(selector.options[selector.current_option])
	$ShipEditor.visible = false
	emit_signal("done")

func is_editing():
	return $ShipEditor.visible

func reset():
	equipped = [1, 1, 0, 0]
