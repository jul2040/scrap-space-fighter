extends Node

const max_scoreboard_size = 8

var sfx = [
	preload("res://assets/sfx/Enemy_Die_1.wav"), #0
	preload("res://assets/sfx/Enemy_Hit_2.wav"), #1
	preload("res://assets/sfx/Hit_1.wav"), #2
	preload("res://assets/sfx/Laser_Beam.wav"), #3
	preload("res://assets/sfx/Shoot_1.wav"), #4
	preload("res://assets/sfx/Space_Atmosphere.wav"), #5
	preload("res://assets/sfx/Thrust_3.wav") #6
]

var difficulty = 0
var score = 0 setget set_score
var asteroid_scene = preload("res://game/enemies/Asteroid.tscn")

signal score_changed
func set_score(new):
	score = new
	emit_signal("score_changed")
