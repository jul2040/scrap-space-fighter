extends Node

var part_info = [
	{"name":"Empty", "desc":"", "scene":null, "texture":null},
	{"name":"Space Shooter", "desc":"A basic shooter.", "scene":preload("res://game/parts/Shooter.tscn"), "texture":preload("res://assets/textures/gun.png")},
	{"name":"Machine Gun", "desc":"Shoots very fast.\nAlso very heavy.", "scene":preload("res://game/parts/MachineGun.tscn"), "texture":preload("res://assets/textures/machinegun.png")},
	{"name":"Sniper", "desc":"Shoots slow and hard.", "scene":preload("res://game/parts/Sniper.tscn"), "texture":preload("res://assets/textures/sniper.png")},
	{"name":"Fire Rate", "desc":"Makes your guns shoot faster.\nAlso makes them do less damage.", "scene":preload("res://game/parts/FireRate.tscn"), "texture":preload("res://assets/textures/fire_rate.png")},
	{"name":"Damage", "desc":"Makes your guns shoot harder.\nAlso makes them shoot slower.", "scene":preload("res://game/parts/Damage.tscn"), "texture":preload("res://assets/textures/damage.png")},
	{"name":"Engine", "desc":"Makes your ship go zoom zoom.\nAlso makes it turn slightly slower.", "scene":preload("res://game/parts/Movement.tscn"), "texture":preload("res://assets/textures/engine.png")},
	{"name":"RCS Thruster", "desc":"Makes your ship turn faster.\nAlso makes it go slightly slower.", "scene":preload("res://game/parts/Turning.tscn"), "texture":preload("res://assets/textures/engine.png")},
	{"name":"Laser", "desc":"Shoots a beam of death.\nNeeds to cool down.", "scene":preload("res://game/parts/Laser.tscn"), "texture":preload("res://assets/textures/laser.png")},
	{"name":"Mine Thrower", "desc":"Throws a mine that will explode on contact.", "scene":preload("res://game/parts/MineShooter.tscn"), "texture":preload("res://assets/textures/minethrower.png")},
	{"name":"Sword", "desc":"Big anime sword.", "scene":preload("res://game/parts/Sword.tscn"), "texture":preload("res://assets/textures/sword.png")}
]

func _ready():
	randomize()

func get_random_part():
	return (randi()%(len(part_info)-1))+1

func get_part_name(p:int):
	return part_info[p]["name"]

func get_desc(p:int):
	return part_info[p]["desc"]

func get_scene(p:int):
	return part_info[p]["scene"]

func get_texture(p:int):
	return part_info[p]["texture"]
