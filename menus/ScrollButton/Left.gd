extends Control

export(Color) var color:Color = Color(1, 1, 1)
export(float) var width:float = 4
export(bool) var flip_h:bool = false
export(float) var h_margin:float = 14
export(PoolVector2Array) var points:PoolVector2Array = [
		Vector2(1, 1),
		Vector2(1, -1),
		Vector2(-1, 0),
		Vector2(1, 1)]

func _draw():
	var p = points
	var scale = min(rect_size.x/6, rect_size.y/3)
	for i in range(len(points)):
		p[i] *= scale
	for i in range(len(p)):
		if(flip_h):
			p[i].x += rect_size.x - scale - h_margin
		else:
			p[i].x += scale + h_margin
		p[i].y += rect_size.y/2
	$Highlight.points = p
	draw_polyline(p, color, width, true)

func _ready():
	if(flip_h):
		for i in range(len(points)):
			points[i].x = -points[i].x
	_on_Left_item_rect_changed()

func _on_Left_item_rect_changed():
	update()

signal selected
func _on_Left_focus_entered():
	emit_signal("selected")
	$Highlight.modulate.a = 1
