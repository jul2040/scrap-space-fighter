extends Control

var points:PoolVector2Array setget set_points

func set_points(arrow):
	points = []
	points.append(arrow[1])
	points.append(0.5*(arrow[0]+arrow[2]))
	
	points.append(0.5*(arrow[1]+arrow[2]))
	points.append((0.25*arrow[0]+0.75*arrow[2]))
	
	points.append(0.5*(arrow[0]+arrow[1]))
	points.append((0.75*arrow[3]+0.25*arrow[2]))
	if(get_parent().flip_h):
		for i in range(len(points)):
			points[i].x -= get_parent().rect_size.x/2
	update()

func _draw():
	for i in range(0, len(points)-1, 2):
		draw_line(points[i], points[i+1], get_parent().color, get_parent().width, true)

func _process(delta):
	modulate.a -= delta*5
