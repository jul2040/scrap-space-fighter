extends Button

export(PoolIntArray) var options:PoolIntArray = [] setget set_options

export(int) var current_option:int = 0 setget set_option

func _ready():
	self.current_option = 0

func _on_Left_selected():
	grab_focus()
	self.current_option -= 1
	emit_signal("selected_changed", current_option)

func _on_Right_selected():
	grab_focus()
	self.current_option += 1
	emit_signal("selected_changed", current_option)

signal selected_changed(new)
func set_option(new):
	if(len(options) == 0):
		return
	var dir = new-current_option
	current_option = posmod(new, len(options))
	if(options[current_option] != 0):
		for selector in get_node("../../..").selectors:
			if(selector != self and selector.current_option == current_option):
				set_option(current_option+dir)
				return
	text = PartInfo.get_part_name(options[current_option])

func set_options(new):
	options = new

func _on_ScrollButton_pressed():
	_on_Right_selected()

func get_desc():
	return PartInfo.get_desc(options[current_option])

func get_selected():
	return options[current_option]
