extends Control

onready var letter_buttons = [$LetterButton1, $LetterButton2, $LetterButton3]
var nick = ""
func _ready():
	$ScoreLabel.text = "%06d"%GameInfo.score
	letter_buttons[0].grab_focus()
	for b in letter_buttons:
		b.connect("pressed", self, "on_letter_pressed", [b])

func on_letter_pressed(button):
	nick += button.letter
	var ind = letter_buttons.find(button)+1
	if(ind < len(letter_buttons)):
		letter_buttons[ind].grab_focus()
	else:
		var scoreboard = Serializer.get("scoreboard")
		if(scoreboard == null):
			scoreboard = []
		var insert_ind = len(scoreboard)
		for s in range(len(scoreboard)):
			if(GameInfo.score < scoreboard[s]["score"]):
				insert_ind = s
				break
		scoreboard.insert(insert_ind, {"nick":nick, "score":GameInfo.score})
		if(len(scoreboard) > GameInfo.max_scoreboard_size):
			scoreboard.remove(0)
		Serializer.set("scoreboard", scoreboard)
		get_tree().change_scene("res://menus/Scoreboard.tscn")
