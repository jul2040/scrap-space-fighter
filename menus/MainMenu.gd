extends Control

func _ready():
	GameInfo.score = 0
	MusicManager.music(2)
	#we need to instance and delete the controls menu
	#so that the controls are set
	var controls = preload("res://menus/Controls.tscn").instance()
	controls.visible = false
	add_child(controls)
	controls.queue_free()
	#put the focus on play
	$MainMenu/Play.grab_focus()

func _on_Play_pressed():
	GameInfo.difficulty = 0
	get_tree().change_scene("res://game/Level.tscn")

func _on_Controls_pressed():
	get_tree().change_scene("res://menus/Controls.tscn")

func _on_SwapTimer_timeout():
	$MainMenu.visible = !$MainMenu.visible
	$Scoreboard.visible = !$MainMenu.visible
	if($MainMenu.visible):
		$MainMenu/Play.grab_focus()

func _input(event):
	if(event is InputEventKey and !event.pressed):
		if($Scoreboard.visible):
			_on_SwapTimer_timeout()
		$SwapTimer.start()
