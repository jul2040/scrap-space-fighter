extends Button

var letter = 'A'
func _on_Up_focus_entered():
	grab_focus()
	letter = char(wrapi(ord(letter)+1, ord('A'), ord('Z')+1))
	text = letter

func _on_Down_focus_entered():
	grab_focus()
	letter = char(wrapi(ord(letter)-1, ord('A'), ord('Z')+1))
	text = letter
