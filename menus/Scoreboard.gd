extends Control

func _ready():
	var scoreboard = Serializer.get("scoreboard").duplicate()
	for s in range(len(scoreboard)/2):
		var tmp = scoreboard[s]
		scoreboard[s] = scoreboard[len(scoreboard)-1-s]
		scoreboard[len(scoreboard)-1-s] = tmp
	for score in scoreboard:
		var entry = preload("Score.tscn").instance()
		entry.score = score["score"]
		entry.nick = score["nick"]
		$Scores.add_child(entry)

var ready = false
func _on_ReadyTimer_timeout():
	ready = true

export(bool) var demo = false

func _input(event):
	if(demo):
		return
	if(not ready):
		return
	if(event is InputEventKey and event.pressed):
		get_tree().change_scene("res://menus/MainMenu.tscn")
