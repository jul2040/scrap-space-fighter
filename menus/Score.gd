extends HBoxContainer

var nick setget set_nick

func set_nick(new):
	nick = new
	$NickLabel.text = new

var score setget set_score

func set_score(new):
	score = new
	$ScoreLabel.text = "%06d"%new
