extends Control

func _input(event):
	if(not ready):
		return
	if(event is InputEventKey and event.pressed):
		if(Serializer.get("scoreboard") == null):
			Serializer.set("scoreboard", [])
		if(len(Serializer.get("scoreboard")) < GameInfo.max_scoreboard_size or GameInfo.score > Serializer.get("scoreboard")[0]["score"]):
			get_tree().change_scene("res://menus/ScoreboardEntry.tscn")
		else:
			get_tree().change_scene("res://menus/MainMenu.tscn")

var ready = false
func _on_Timer_timeout():
	ready = true
	$Label2.visible = true
